#!/bin/bash
for PID in $(pgrep chrome);
	do test=$(ps --no-headers -o maj_flt "$PID")
	if [ "$test" -gt 1000 ]
	then
		echo "Chrome $PID har for˚arsaket $test major page faults (mer enn 1000!)"
	else 
		echo "Chrome $PID har for˚arsaket $test major page faults."
	fi
done
