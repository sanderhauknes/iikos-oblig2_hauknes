#!/bin/bash

dir=$1;
fullness=$(df "$dir" | sed -n 2p | awk '{print $5}');
noOfFiles=$(find . "$dir" -type f | wc -l);
largestFileSize=$(find . "$dir" -printf '%s %p\n'|sort -nr|head -1 | awk '{print $1}');
largestFilePath=$(find . "$dir" -printf '%s %p\n'|sort -nr|head -1 | awk '{print $2}');
avgFileSize=$(find "$dir" . -printf '%s %p\n'|awk '{sum+=$1; c++} END{print sum/c}');
hardLinks=$(find . "$dir" -type f -printf '%p\n' | sort -nr | head -n 1);
noOfHardLinks=$(find . "$dir" -type f -printf '%n\n' | sort -nr | head -n 1);
echo "Partisjonen $dir befinner seg p˚a er $fullness full";
echo "Det finnes $noOfFiles filer."
echo "Den største er $largestFilePath som er $largestFileSize stor.";
echo "Gjennomsnittlig filstørrelse er $avgFileSize bytes.";
echo "Filen $hardLinks har flest hardlinks, den har $noOfHardLinks.";
