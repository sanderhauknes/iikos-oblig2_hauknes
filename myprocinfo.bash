#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg?"
 echo "  2 - Hvor lenge er det siden siste boot?"
 echo "  3 - Hva var gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tr˚ader finnes?"
 echo "  5 - Hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    read -r
    clear
    ;;
  2)clear
    oppetid=$(uptime | awk '{print $3}')
    echo "Det $oppetid dager siden siste boot."
    read -r
    clear
    ;;
  3)clear
    load=$(uptime | awk '{print $10}')
    echo "Load siste minutt:$load"
    read -r
    clear
    ;;  
  4)clear
    prosesser="$(ps aux | wc -l)"
    echo "$prosesser prosesser kjører"
    read -r
    clear
    ;;    
  5)clear
    switcher="$(grep ctxt /proc/stat | awk '{print $2}')"
    echo "Det er $switcher context-switcher"
    read -r
    clear
    ;; 
  6)clear
    interupt="$(grep intr /proc/stat | awk '{print $2}')"
    echo "Det er $interupt interupts"
    read -r
    clear
    ;; 
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done

