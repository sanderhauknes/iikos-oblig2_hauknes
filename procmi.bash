#!/bin/bash

dato=$(date +%Y%m%d-%H:%M:%S)
PID=$1
echo "$PID"
VmSize=$(grep VmSize /proc/"$PID"/status | awk '{print $2}');
VmData=$(grep VmData /proc/"$PID"/status | awk '{print $2}');
VmStk=$(grep VmStk /proc/"$PID"/status | awk '{print $2}');
VmExe=$(grep VmExe /proc/"$PID"/status | awk '{print $2}');
VmLib=$(grep VmLib /proc/"$PID"/status | awk '{print $2}');
VmRSS=$(grep VmRSS /proc/"$PID"/status | awk '{print $2}');
VmPTE=$(grep VmPTE /proc/"$PID"/status | awk '{print $2}');


{
echo "******** Minne info om prosess med PID $PID ********" 
echo "Total bruk av virtuelt minne (VmSize): $VmSize kb" 
echo "Mengde privat virtuelt minne (VmData+VmStk+VmExe): $((VmData+VmStk+VmExe)) kb" 
echo "Mengde shared virtuelt minne (VmLib): $VmLib kb"
echo "Total bruk av fysisk minne (VmRSS): $VmRSS kb" 
echo "Mengde fysisk minne som benyttes til page table (VmPTE): $VmPTE kb"
} >> "$PID"-"$dato".meminfo;


# echo "Eksempel på datoformat: $dato"
